const EventEmitter = require('events')
const fs = require('fs')
const {promisify} = require('util')

const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const clone = require('clone')
const traverse = require('object-traverse')
const flat = require('flat')

class Subconfig{
	constructor(config, key){
		this.config = config
		this.key = key
	}
	_key(key){return key === undefined? this.key: this.key+'.'+key}
	async set(key, value, save){
		await this.config.set(this._key(key), value, save)
		return this
	}
	get(key){
		return this.config.get(this._key(key))
	}
	async remove(key){
		await this.config.remove(this._key(key))
		return this
	}
	async default(key, value){
		await this.config.default(this._key(key), value)
		return this
	}
	getSub(key){
		return new Subconfig(this.config, this._key(key))
	}
}

class Config extends EventEmitter{
	constructor(file='config.json', save=true){
		super()
		this.file = file
		this.defaultSave = save
		this.defaults = {}
		this.raw = undefined
	}
	async load(file){
		if(file) this.file = file
		this.raw = JSON.parse(await readFile(this.file, 'utf8').catch(err=>{
			if(err.code === 'ENOENT') return '{}'
			throw err
		}))
		this.loaded = true
		await this._syncDefaults()
		return this
	}
	async save(){
		if(this.saving){
			if(!this.qSave){
				this.qSave = this.saving.then(()=>{
					delete this.qSave
					this.save()
				})
			}
			return this
		}
		await (this.saving = writeFile(this.file, JSON.stringify(clone(this.raw || {}), null, '\t'), 'utf8').then(data=>{
			delete this.saving
			return data
		}, err=>{
			delete this.saving
			throw err
		}))
		return this
	}
	async clear(save=this.loaded && this.defaultSave){
		this.raw = {}
		this.loaded = false
		await this._syncDefaults(false)
		if(save) await this.save()
		return this
	}
	_set(object, key, value){
		if(key.match(/\./)){ //check if parent exists
			let path = key.split('.')
			let parent = path.slice(0, path.length-1).join('.')
			if(parent && !traverse(object).has(parent))
				traverse(object).create(parent)
		}
		traverse(object).set(key, clone(value))
	}
	async set(key, value, save=this.loaded && this.defaultSave){
		this.raw = this.raw || {}
		this._set(this.raw, key, value)
		if(save) await this.save()
		return this
	}
	get(key){
		if(key === undefined) return this.raw
		return traverse(this.raw).get(key)
	}
	async remove(key, save=this.loaded && this.defaultSave){
		traverse(this.raw).delete(key)
		await this._syncDefault(key, false)
		if(save) await this.save()
		return this
	}
	
	async default(key, value, save=this.loaded && this.defaultSave){
		this._set(this.defaults, key, value)
		await this._syncDefault(key, save)
		return this
	}
	clearDefaults(){
		this.defaults = {}
		return this
	}
	async _syncDefault(key, save=this.loaded && this.defaultSave){
		let value = traverse(this.defaults).get(key)
		if(value !== undefined && !traverse(this.raw).has(key))
			await this.set(key, value, save)
		return this
	}
	async _syncDefaults(save=this.loaded && this.defaultSave){
		let flattened = flat(this.defaults)
		for(let f in flattened) await this._syncDefault(f, false)
		if(save) await this.save()
		return this
	}
	getSub(key){return new Subconfig(this, key)}
}

const config = new Config()
config.Config = Config
module.exports = config
